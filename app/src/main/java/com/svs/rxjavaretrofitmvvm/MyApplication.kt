package com.svs.rxjavaretrofitmvvm

import android.app.Application
import com.svs.rxjavaretrofitmvvm.di.DaggerAppComponent


class MyApplication : Application() {
    val component = DaggerAppComponent.create()
}