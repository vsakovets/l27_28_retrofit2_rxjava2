package com.svs.rxjavaretrofitmvvm.data

class Post {
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var completed: Boolean = false
}