package com.svs.rxjavaretrofitmvvm.data

import com.svs.rxjavaretrofitmvvm.retrofit.ResourcesApi
import io.reactivex.Observable
import javax.inject.Inject

class Repository @Inject constructor(
    private val resourcesApi: ResourcesApi
) {
    fun getPost(): Observable<List<Post>> {
        return resourcesApi.fetchAllUsers()
    }
    fun getUser(userId: Int): Observable<User> {
        return resourcesApi.fetchUserById(userId)
    }
}