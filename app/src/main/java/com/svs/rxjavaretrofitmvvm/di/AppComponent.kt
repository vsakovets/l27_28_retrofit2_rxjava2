package com.svs.rxjavaretrofitmvvm.di


import com.svs.rxjavaretrofitmvvm.data.Repository
import dagger.Component

@Component(modules = [RetrofitModule::class])
interface AppComponent {
    fun getRepo(): Repository

}