package com.svs.rxjavaretrofitmvvm.di

import com.svs.rxjavaretrofitmvvm.retrofit.ResourcesApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object RetrofitModule {
    private val BASEURL = "https://jsonplaceholder.typicode.com/"
    @Provides
    fun provideRetrofit(): ResourcesApi {

        return Retrofit.Builder()
            .baseUrl(BASEURL)
            .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
            .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
            .build()
            .create(ResourcesApi::class.java)
    }
}