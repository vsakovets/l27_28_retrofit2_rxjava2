package com.svs.rxjavaretrofitmvvm.retrofit

import com.svs.rxjavaretrofitmvvm.data.Post
import com.svs.rxjavaretrofitmvvm.data.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface ResourcesApi {
    @GET("posts")
    fun fetchAllUsers(): Observable<List<Post>>

    @GET("users/{id}")
    fun fetchUserById(@Path("id") id: Int): Observable<User>

}