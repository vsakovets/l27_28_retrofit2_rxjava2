package com.svs.rxjavaretrofitmvvm.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.svs.rxjavaretrofitmvvm.R
import com.svs.rxjavaretrofitmvvm.ui.adapters.PostAdapter
import com.svs.rxjavaretrofitmvvm.data.Post
import com.svs.rxjavaretrofitmvvm.viewModel.PostViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val viewModel by lazy {
        ViewModelProviders.of(this).get(PostViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
        viewModel.fetchData()

    }

    private fun setupView() {
        recycler_posts.setHasFixedSize(true)
        recycler_posts.layoutManager = LinearLayoutManager(this)
        val adapter = PostAdapter()
        recycler_posts.adapter = adapter
        viewModel.listPost.observe(this,
            Observer<List<Post>?> { t ->
                t?.let {
                    adapter.setNewList(it)
                }
            })
    }

}
