package com.svs.rxjavaretrofitmvvm.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.svs.rxjavaretrofitmvvm.R
import com.svs.rxjavaretrofitmvvm.data.User
import com.svs.rxjavaretrofitmvvm.viewModel.PostViewModel
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {
    val viewModel by lazy {
        ViewModelProviders.of(this).get(PostViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        val userId = intent.getIntExtra("userId", 1)
        viewModel.fetchUser(userId)
        setupView()
    }

    private fun setupView() {
        viewModel.user.observe(
            this,
            Observer<User> { t ->
                t?.let {
                    username.text = it.username
                    name.text = it.name
                    address.text = it.address.toString()
                    company.text = it.company.toString()
                    email.text = it.email
                    phone.text = it.phone
                    website.text = it.website

                }
            })
    }
}
