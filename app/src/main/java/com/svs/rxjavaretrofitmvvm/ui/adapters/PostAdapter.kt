package com.svs.rxjavaretrofitmvvm.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.svs.rxjavaretrofitmvvm.R
import com.svs.rxjavaretrofitmvvm.data.Post
import com.svs.rxjavaretrofitmvvm.data.User


class PostAdapter :
    RecyclerView.Adapter<PostViewHolder>() {
    private var postList: List<Post> = emptyList()
    private var user: User? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.post_layout, parent, false)
        return PostViewHolder(itemView)
    }

    fun setNewList(newList: List<Post>) {
        postList = newList
        notifyDataSetChanged()
    }

    fun setUser(user: User) {
        this.user = user
    }

    override fun getItemCount(): Int {
        return postList.size
    }


    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(postList[position])

    }


}