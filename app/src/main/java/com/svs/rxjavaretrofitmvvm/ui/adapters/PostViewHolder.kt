package com.svs.rxjavaretrofitmvvm.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.svs.rxjavaretrofitmvvm.R
import com.svs.rxjavaretrofitmvvm.data.Post
import com.svs.rxjavaretrofitmvvm.ui.UserActivity
import com.svs.rxjavaretrofitmvvm.utils.ColorUtils
import kotlinx.android.synthetic.main.post_layout.view.*

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val img_id = itemView.img_id
    val txt_id = itemView.txt_id
    val txt_title = itemView.txt_title
    val txt_user = itemView.txt_user
    val chip = itemView.chip
    val cv_user_item = itemView.cv_user_item

    fun bind(post: Post) {
        val color = ColorUtils()
        txt_id.text = post.id.toString()
        img_id.setColorFilter(color.getColor(post.id))
        chipCheck(post, position)
        chip.setOnClickListener {
            post.completed = !post.completed
            chipCheck(post, position)
        }
        txt_title.text = post.title
        txt_user.text =
            StringBuilder(chip.context.getString(R.string.assignee_user) + post.userId.toString())

        cv_user_item.setOnClickListener {
            navigateToUserActivity(post, itemView.context)
        }

    }

    fun navigateToUserActivity(post: Post, context: Context) {
        val userId = post.userId
        val intent = Intent(context, UserActivity::class.java)
        intent.putExtra("userId", userId)
        startActivity(context, intent, null)
    }

    fun setChipChecked() {
        chip.setChipBackgroundColorResource(R.color.colorChipChecked)
        chip.text = chip.context.getString(R.string.done)
        chip.isChecked = true
    }

    fun setChipUnchecked() {
        chip.setChipBackgroundColorResource(R.color.colorChipUnchecked)
        chip.text = chip.context.getString(R.string.todo)
        chip.isChecked = false
    }


    fun chipCheck(postList: Post, position: Int) {
        if (postList.completed) {
            setChipChecked()
        } else {
            setChipUnchecked()
        }
    }


}