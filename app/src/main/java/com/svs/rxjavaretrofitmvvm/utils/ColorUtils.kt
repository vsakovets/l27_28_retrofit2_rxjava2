package com.svs.rxjavaretrofitmvvm.utils

import android.os.Build

class ColorUtils {

    var color: Int = 0

    fun getColor(id: Int): Int {
        val startValue: Float = 255F
        var r: Float = (255 - id * 2).toFloat()
        var g: Float = (255 - id * 4).toFloat()
        var b: Float = (255 - id * 8).toFloat()
        if (r <= 0) {
            r = startValue
        } else if (g <= 0) {
            g = startValue
        } else if (b <= 0) {
            b = startValue
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            color = android.graphics.Color.rgb(r, g, b)
        }

        return color
    }


}