package com.svs.rxjavaretrofitmvvm.viewModel

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.svs.rxjavaretrofitmvvm.MyApplication
import com.svs.rxjavaretrofitmvvm.data.Post
import com.svs.rxjavaretrofitmvvm.data.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class PostViewModel(application: Application) : AndroidViewModel(application) {
    private val repo by lazy {
        (application as MyApplication).component.getRepo()
    }

    private val _listPost: MutableLiveData<List<Post>> = MutableLiveData()
    val listPost: LiveData<List<Post>> = _listPost

    private val _user: MutableLiveData<User> = MutableLiveData()
    val user: LiveData<User> = _user

    fun fetchData() {
        repo.getPost().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : io.reactivex.Observer<List<Post>?> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(posts: List<Post>) {
                    _listPost.value = posts
                }

                override fun onError(e: Throwable) {
                    showError(e)
                }
            })
    }


    fun fetchUser(userId: Int) {
        repo.getUser(userId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : io.reactivex.Observer<User> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(user: User) {
                    _user.value = user
                }

                override fun onError(e: Throwable) {
                    showError(e)
                }

            })
    }

    fun showError(e: Throwable) {
        Toast.makeText(getApplication(), "Error: $e", Toast.LENGTH_LONG)
            .show()
    }

}